public class TestCircle {

	public static void main(String[] args) {
		// System.out.println("Circle.numberOfObjects = " + Circle.numberOfObjects);

		Circle c1 = new Circle(3.0);
		Circle c2 = new Circle(5.0);

		System.out.println("c1.radius = " + c1.radius);
		System.out.println("c2.radius = " + c2.radius);

		System.out.println("c1.numberOfObjects = " + c1.numberOfObjects);
		System.out.println("c2.numberOfObjects = " + c2.numberOfObjects);
		System.out.println("Circle.numberOfObjects = " + Circle.numberOfObjects);
	}

}