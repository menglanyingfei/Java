public class Circle {

	double radius;

	Circle() {
		radius = 1.0;
	}

	double getArea() {
		return radius * radius * Math.PI;
	}

}