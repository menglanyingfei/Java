public class Circle {

	static int numberOfObjects = 0;
	double radius;
	
	Circle(double newRadius) {
		radius = newRadius;
		numberOfObjects++;
	}
	
	static int getNumberOfObjects() {
		return numberOfObjects;
	}

	double getArea() {
		return radius * radius * Math.PI;
	}

}