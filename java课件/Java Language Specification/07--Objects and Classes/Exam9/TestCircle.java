public class TestCircle {

	public static void main(String[] args) {
		System.out.println(Circle.getNumberOfObjects());
		
		Circle c1 = new Circle(3.0);
		Circle c2 = new Circle(5.0);
		
		System.out.println("c1.radius = " + c1.radius);
		System.out.println("c2.radius = " + c2.radius);
		
		System.out.println(c1.getNumberOfObjects());
		System.out.println(c2.getNumberOfObjects());
		System.out.println(Circle.getNumberOfObjects());
	}

}