public class CirclePerimeter extends Circle {

	public double getPerimeter() {
		return 2 * Math.PI * getRadius();
	}
	
}