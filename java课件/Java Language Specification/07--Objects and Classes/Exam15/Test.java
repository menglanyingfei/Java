public class Test {

	public static void main(String[] args) {
		CircleBlank circleBlank = new CircleBlank();
		circleBlank.setRadius(10);
		System.out.println("radius = " + circleBlank.getRadius());
		System.out.println("area = " + circleBlank.getArea());
		
		System.out.println();
		
		CirclePerimeter circlePerimeter = new CirclePerimeter();
		circlePerimeter.setRadius(1);
		System.out.println("radius = " + circlePerimeter.getRadius());
		System.out.println("area = " + circlePerimeter.getArea());
		System.out.println("Perimeter = " + circlePerimeter.getPerimeter());
	}

}