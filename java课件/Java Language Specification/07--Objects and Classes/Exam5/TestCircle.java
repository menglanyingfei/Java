public class TestCircle {

	public static void main(String[] args) {
		Circle circle1 = new Circle();
		System.out.println("area1 = " + circle1.getArea());
		
		Circle circle2 = new Circle(10.0);
		System.out.println("area2 = " + circle2.getArea());
	}

}