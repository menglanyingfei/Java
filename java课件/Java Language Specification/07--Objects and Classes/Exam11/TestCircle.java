public class TestCircle {

	public static void main(String[] args) {
		Circle c = new Circle(3.0);
		System.out.println("c.getRadius() = " + c.getRadius());
		System.out.println("c.getArea() = " + c.getArea());
		
		c.setRadius(5.0);
		System.out.println("c.getRadius() = " + c.getRadius());
		System.out.println("c.getArea() = " + c.getArea());
	}

}