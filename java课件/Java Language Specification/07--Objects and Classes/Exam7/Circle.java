public class Circle {

	double radius;

	Circle(double r) {
		radius = r;
	}

	double getArea() {
		return radius * radius * Math.PI;
	}

}