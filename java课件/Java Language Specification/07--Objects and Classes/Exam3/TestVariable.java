public class TestVariable {

	public static void main(String[] args) {
		int x = 3, y = 5;

		System.out.println("x = " + x);
		System.out.println("y = " + y);

		x = y;
		x++;
		System.out.println("x = " + x);
		System.out.println("y = " + y);

		Circle c1 = new Circle();
		c1.radius = 3.0;
		Circle c2 = new Circle();
		c2.radius = 5.0;

		System.out.println("c1.radius = " + c1.radius);
		System.out.println("c2.radius = " + c2.radius);

		c1 = c2;
		c1.radius++;
		System.out.println("c1.radius = " + c1.radius);
		System.out.println("c2.radius = " + c2.radius);
	}

}