public class Circle {

	private static int numberOfObjects = 0;
	private double radius;
	
	public Circle(double newRadius) {
		radius = newRadius;
		numberOfObjects++;
	}
	
	public static int getNumberOfObjects() {
		return numberOfObjects;
	}
	
	public double getRadius() {
		return radius;
	}

	public void setRadius(double newRadius) {
		radius = (newRadius >= 0) ? newRadius : 0;
	}

	public double getArea() {
		return radius * radius * Math.PI;
	}

}