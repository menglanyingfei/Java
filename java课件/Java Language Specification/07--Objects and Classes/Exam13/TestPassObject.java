public class TestPassObject {
	
	public static void method(Circle c, int n) {
		c.setRadius(10);
		n = 10;
	}

	public static void main(String[] args) {
		Circle circle = new Circle(5);
		int number = 5;
		method(circle, number);
		System.out.println("circle.getRadius() = " + circle.getRadius());
		System.out.println("number = " + number);
		
		Circle c = new Circle(5);
		int n = 5;
		method(c, n);
		System.out.println("c.getRadius() = " + c.getRadius());
		System.out.println("n = " + n);
	}

}
