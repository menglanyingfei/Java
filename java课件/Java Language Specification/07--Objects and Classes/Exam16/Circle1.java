
public class Circle1 extends Circle {

	public double getArea() {
		double area = Math.PI * getRadius() * getRadius();
		area = (int) (area * 10) / 10.0;
		
		return area;
	}
}