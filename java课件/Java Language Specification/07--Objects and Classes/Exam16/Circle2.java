public class Circle2 extends Circle1 {

	public double getArea() {
		double area = Math.PI * getRadius() * getRadius();
		area = (int) (area * 100) / 100.0;

		return area;
	}
}