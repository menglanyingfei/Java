public class Test {

	public static void main(String[] args) {
		Circle circle;

		circle = new Circle();
		circle.setRadius(10);
		System.out.println("radius = " + circle.getRadius());
		System.out.println("area = " + circle.getArea());
		System.out.println();

		circle = new Circle1();
		circle.setRadius(10);
		System.out.println("radius = " + circle.getRadius());
		System.out.println("area = " + circle.getArea());
		System.out.println();

		circle = new Circle2();
		circle.setRadius(10);
		System.out.println("radius = " + circle.getRadius());
		System.out.println("area = " + circle.getArea());
	}

}