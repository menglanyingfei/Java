import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.stage.Stage;


public class Step12 extends Application {
	
	private int width = 600, height = 600;
	
	private Circle circle = new Circle(width / 2, height / 2, 30);
	
	private Button buttonEnlarge = new Button("放大");
	private Button buttonShrink = new Button("缩小");

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		primaryStage.setTitle("Step12");
		primaryStage.setResizable(false);
		primaryStage.setScene(initUI());
		primaryStage.sizeToScene();
		primaryStage.centerOnScreen();
		
		addEventHandlers();
		
		primaryStage.show();
	}
	
	private Scene initUI() {
		Pane pane = new Pane();
		pane.setPrefWidth(width);
		pane.setPrefHeight(height);
		pane.setStyle("-fx-background-color: cornsilk");
		
		Line lineX = new Line(10, 10, width - 10, 10);
		Line lineX1 = new Line(width - 20, 5, width - 10, 10);
		Line lineX2 = new Line(width - 20, 15, width - 10, 10);
		pane.getChildren().addAll(lineX, lineX1, lineX2);
		
		Line lineY = new Line(10, 10, 10, height - 10);
		Line lineY1 = new Line(5, height - 20, 10, height - 10);
		Line lineY2 = new Line(15, height - 20, 10, height - 10);
		pane.getChildren().addAll(lineY, lineY1, lineY2);
		
		circle.setStroke(Color.RED);
		circle.setFill(Color.RED);
		pane.getChildren().add(circle);
		
		HBox hBox = new HBox(100, buttonEnlarge, buttonShrink);
		hBox.setAlignment(Pos.CENTER);
		hBox.setStyle("-fx-background-color: lightblue");
		hBox.setPadding(new Insets(5, 5, 5, 5));
		
		BorderPane borderPane = new BorderPane();
		borderPane.setStyle("-fx-font-size: 28px");
		borderPane.setCenter(pane);
		borderPane.setBottom(hBox);
		
		Scene scene = new Scene(borderPane);
		
		return scene;
	}
	
	private void addEventHandlers() {
		// Lambda Expressions (λ表达式)
		buttonEnlarge.setOnAction(e -> {
			if (circle.getRadius() <= 280) {
				circle.setRadius(circle.getRadius() + 10);
			}
		});
		
		buttonShrink.setOnAction(e -> {
			if (circle.getRadius() >= 20) {
				circle.setRadius(circle.getRadius() - 10);
			}
		});
	}

}