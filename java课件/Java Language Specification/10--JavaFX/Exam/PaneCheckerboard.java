import javafx.scene.input.MouseButton;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;


public class PaneCheckerboard extends Pane {
	
	private int length = 600; // �߳�
	private int margin = 20; // �߾�
	private int interval = (length - margin * 2) / 14; // ���
	private int radius = interval / 2 - 2; // ���ӵİ뾶
	private int[][] chessPieces = new int[15][15]; // ������ÿ��λ�õ�״̬��0���塢1���塢2������
	private int curChessPiece = 0; // ��ǰ���ӣ�0���塢1����
	
	public PaneCheckerboard() {
		super();
		
		setPrefSize(length, length);
		
		setStyle("-fx-background-color: blanchedalmond");
		
		for (int i = 0; i < 15; i++) {
			for (int j = 0; j < 15; j++) {
				chessPieces[i][j] = 2;
			}
		}
		
		paintGrid();
		
		setOnMouseClicked(e -> {
			if (e.getButton() == MouseButton.PRIMARY) {
				int x = (int) (e.getX());
				int y = (int) (e.getY());
				paintChessPieces(x, y);
			}
		});
	}
	
	// ������
	private void paintGrid() {
		for (int i = 0; i < 15; i++) {
			Line lineH = new Line(margin, margin + i * interval, length - margin, margin + i * interval);
			getChildren().add(lineH);
			
			Line lineV = new Line(margin + i * interval, margin, margin + i * interval, length - margin);
			getChildren().add(lineV);
		}
		
		Circle[] circles = new Circle[5];
		circles[0] = new Circle(margin + interval * 3, margin + interval * 3, radius / 2);
		circles[1] = new Circle(margin + interval * 3, margin + interval * 11, radius / 2);
		circles[2] = new Circle(margin + interval * 7, margin + interval * 7, radius / 2);
		circles[3] = new Circle(margin + interval * 11, margin + interval * 3, radius / 2);
		circles[4] = new Circle(margin + interval * 11, margin + interval * 11, radius / 2);
		getChildren().addAll(circles);
	}
	
	// ������
	private void paintChessPieces(int x, int y) {
		for (int i = (x - margin) / interval; i <= (x - margin) / interval + 1; i++) {
			for (int j = (y - margin) / interval; j <= (y - margin) / interval + 1; j++) {
				if ((x - (margin + i * interval)) * (x - (margin + i * interval)) + (y - (margin + j * interval)) * (y - (margin + j * interval)) <= radius * radius) {
					if (chessPieces[i][j] == 2) {
						chessPieces[i][j] = curChessPiece;
						Circle circle = new Circle(margin + interval * i,
								margin + interval * j, radius);
						if (chessPieces[i][j] == 0) {
							circle.setFill(Color.BLACK);
							circle.setStroke(Color.BLACK);
						} else if (chessPieces[i][j] == 1) {
							circle.setFill(Color.WHITE);
							circle.setStroke(Color.WHITE);
						}
						getChildren().add(circle);

						curChessPiece = (curChessPiece + 1) % 2;
					}
					break;
				}
			}
		}
	}

}
