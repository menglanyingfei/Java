import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class Step06 extends Application {

	private TextField textFieldSno = new TextField();
	private TextField textFieldName = new TextField();
	private ComboBox<String> comboBoxGender = new ComboBox<String>();
	private DatePicker datePickerBirthday = new DatePicker();

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		primaryStage.setTitle("Step06");
		primaryStage.setResizable(false);
		primaryStage.setScene(initUI());
		primaryStage.sizeToScene();
		primaryStage.centerOnScreen();
		
		primaryStage.show();
		
		// 调整宽度
		comboBoxGender.setPrefWidth(textFieldName.getWidth());
	}
	
	private Scene initUI() {
		comboBoxGender.getItems().add("男");
		comboBoxGender.getItems().add("女");
		
		datePickerBirthday.setEditable(false);
		
		GridPane gridPane = new GridPane();
		gridPane.setHgap(10);	// 水平间距
		gridPane.setVgap(10);	// 垂直间距
		gridPane.setStyle("-fx-font-size: 24px");	// 字号
		
		gridPane.add(new Label("学号："), 0, 0);
		gridPane.add(textFieldSno, 1, 0);
		gridPane.add(new Label("姓名："), 0, 1);
		gridPane.add(textFieldName, 1, 1);
		gridPane.add(new Label("性别："), 0, 2);
		gridPane.add(comboBoxGender, 1, 2);
		gridPane.add(new Label("生日："), 0, 3);
		gridPane.add(datePickerBirthday, 1, 3);
		
		Scene scene = new Scene(gridPane);
		
		return scene;
	}

}