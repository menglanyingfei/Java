import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class Step03 extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		HBox hBox = new HBox(5);
		hBox.getChildren().add(new ImageView("flower.bmp"));
		hBox.getChildren().add(new ImageView("mooncake.jpg"));
		hBox.getChildren().add(new ImageView("funny.gif"));
		
		Scene scene = new Scene(hBox);
		
		primaryStage.setTitle("Step03");
		primaryStage.setResizable(false);
		primaryStage.setScene(scene);
		primaryStage.sizeToScene();
		primaryStage.centerOnScreen();
		
		primaryStage.show();
	}

}