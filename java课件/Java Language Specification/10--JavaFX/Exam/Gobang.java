import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Gobang extends Application {

	private PaneCheckerboard paneCheckerboard = new PaneCheckerboard();

	public static void main(String[] args) {
		launch(args);
	}
	
	@Override
	public void start(Stage primaryStage) throws Exception {
		Scene scene = new Scene(paneCheckerboard);
		primaryStage.setTitle("������");
		primaryStage.setResizable(false);
		primaryStage.setScene(scene);
		primaryStage.centerOnScreen();
		primaryStage.sizeToScene();
		primaryStage.show();
	}

}