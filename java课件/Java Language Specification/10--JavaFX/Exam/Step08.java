import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class Step08 extends Application {
	
	private Button buttonNum = new Button(" ");
	private Button buttonAdd = new Button("+");
	private Button buttonSubtract = new Button("-");
	private Button buttonMultiply = new Button("*");
	private Button buttonDivide = new Button("/");
	private Button buttonEnter = new Button("=");
	private Button buttonPoint = new Button(".");
	private Button button0 = new Button("0");
	private Button button1 = new Button("1");
	private Button button2 = new Button("2");
	private Button button3 = new Button("3");
	private Button button4 = new Button("4");
	private Button button5 = new Button("5");
	private Button button6 = new Button("6");
	private Button button7 = new Button("7");
	private Button button8 = new Button("8");
	private Button button9 = new Button("9");
	

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		primaryStage.setTitle("Step08");
		primaryStage.setResizable(false);
		primaryStage.setScene(initUI());
		primaryStage.sizeToScene();
		primaryStage.centerOnScreen();
		
		primaryStage.show();
		
		button0.setPrefWidth(button1.getWidth() * 2 + 5);
		buttonAdd.setPrefHeight(button1.getHeight() * 2 + 5);
		buttonEnter.setPrefHeight(button1.getHeight() * 2 + 5);
	}
	
	private Scene initUI() {
		GridPane gridPane = new GridPane();
		gridPane.setHgap(5);
		gridPane.setVgap(5);
		gridPane.setPadding(new Insets(5));
		String value = "-fx-font-family: monospace;"
				+ "-fx-font-size: 48px;"
				+ "-fx-background-color: deepskyblue";
		gridPane.setStyle(value);
		
		gridPane.add(buttonNum, 0, 0);
		gridPane.add(buttonDivide, 1, 0);
		gridPane.add(buttonMultiply, 2, 0);
		gridPane.add(buttonSubtract, 3, 0);
		
		gridPane.add(button7, 0, 1);
		gridPane.add(button8, 1, 1);
		gridPane.add(button9, 2, 1);
		gridPane.add(button4, 0, 2);
		gridPane.add(button5, 1, 2);
		gridPane.add(button6, 2, 2);
		gridPane.add(button1, 0, 3);
		gridPane.add(button2, 1, 3);
		gridPane.add(button3, 2, 3);
		
		gridPane.add(buttonPoint, 2, 4);
		
		gridPane.add(buttonAdd, 3, 1, 1, 2);
		gridPane.add(buttonEnter, 3, 3, 1, 2);
		gridPane.add(button0, 0, 4, 2, 1);
		
		Scene scene = new Scene(gridPane);
		
		return scene;
	}

}