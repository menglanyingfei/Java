import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class Step01 extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		Button button = new Button("ȷ��");
		
		StackPane stackPane = new StackPane();
		stackPane.getChildren().add(button);
		
		Scene scene = new Scene(stackPane, 300, 200);
		
		primaryStage.setTitle("Step01");
		primaryStage.setResizable(false);
		primaryStage.setScene(scene);
		primaryStage.sizeToScene();
		primaryStage.centerOnScreen();
		
		primaryStage.show();
	}

}