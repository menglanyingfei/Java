import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class Step09 extends Application {
	
	private Button buttonTop = new Button("Top");
	private Button buttonBottom = new Button("Bottom");
	private Button buttonLeft = new Button("Left");
	private Button buttonRight = new Button("Right");
	private Button buttonCenter = new Button("Center");
	

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		primaryStage.setTitle("Step09");
		primaryStage.setResizable(false);
		primaryStage.setScene(initUI());
		primaryStage.sizeToScene();
		primaryStage.centerOnScreen();
		
		primaryStage.show();
	}
	
	private Scene initUI() {
		StackPane stackPaneTop = new StackPane(buttonTop);
		stackPaneTop.setStyle("-fx-background-color: yellow");
		stackPaneTop.setPadding(new Insets(10));
		
		StackPane stackPaneBottom = new StackPane(buttonBottom);
		stackPaneBottom.setStyle("-fx-background-color: yellow");
		stackPaneBottom.setPadding(new Insets(10));
		
		StackPane stackPaneLeft = new StackPane(buttonLeft);
		stackPaneLeft.setStyle("-fx-background-color: orange");
		stackPaneLeft.setPadding(new Insets(10));
		
		StackPane stackPaneRight = new StackPane(buttonRight);
		stackPaneRight.setStyle("-fx-background-color: orange");
		stackPaneRight.setPadding(new Insets(10));
		
		StackPane stackPaneCenter = new StackPane(buttonCenter);
		stackPaneCenter.setStyle("-fx-background-color: gold");
		
		BorderPane borderPane = new BorderPane();
		borderPane.setStyle("-fx-font-size: 48px; -fx-background-color: deepskyblue");
		
		borderPane.setTop(stackPaneTop);
		borderPane.setBottom(stackPaneBottom);
		borderPane.setLeft(stackPaneLeft);
		borderPane.setRight(stackPaneRight);
		borderPane.setCenter(stackPaneCenter);
		
		BorderPane.setMargin(stackPaneTop, new Insets(0, 0, 5, 0));
		BorderPane.setMargin(stackPaneCenter, new Insets(0, 5, 0, 5));
		BorderPane.setMargin(stackPaneBottom, new Insets(5, 0, 0, 0));
		
		Scene scene = new Scene(borderPane, 800, 500);
		
		return scene;
	}

}