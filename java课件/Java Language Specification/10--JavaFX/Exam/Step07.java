import javafx.application.Application;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class Step07 extends Application {

	private TextField textFieldUserName = new TextField();
	private PasswordField passwordField = new PasswordField();
	private Button buttonSignIn = new Button("Sign In");

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		primaryStage.setTitle("Step07");
		primaryStage.setResizable(false);
		primaryStage.setScene(initUI());
		primaryStage.sizeToScene();
		primaryStage.centerOnScreen();
		
		primaryStage.show();
	}
	
	private Scene initUI() {
		GridPane gridPane = new GridPane();
		gridPane.setHgap(10);
		gridPane.setVgap(10);
		gridPane.setStyle("-fx-font-size: 24px");
		gridPane.setAlignment(Pos.CENTER);	// 居中
		gridPane.setPadding(new Insets(25));	// 四周填充
		
		Label labelTitle = new Label("Welcome");
		labelTitle.setStyle("-fx-font-size: 48px");
		gridPane.add(labelTitle, 0, 0, 2, 1);	// 跨两列
		
		gridPane.add(new Label("User Name:"), 0, 1);
		gridPane.add(textFieldUserName, 1, 1);
		gridPane.add(new Label("Password:"), 0, 2);
		gridPane.add(passwordField, 1, 2);
		gridPane.add(buttonSignIn, 1, 8);
		
		GridPane.setHalignment(buttonSignIn, HPos.RIGHT); // 右对齐
		
		//gridPane.setGridLinesVisible(true);
		
		Scene scene = new Scene(gridPane, 600, 400);
		//Scene scene = new Scene(gridPane);
		
		return scene;
	}

}