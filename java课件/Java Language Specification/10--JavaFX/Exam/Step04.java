import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Step04 extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		VBox vBox = new VBox(5);
		vBox.getChildren().add(new ImageView("flower.bmp"));
		vBox.getChildren().add(new ImageView("funny.gif"));
		vBox.getChildren().add(new ImageView("landscape.jpg"));
		
		Scene scene = new Scene(vBox);
		
		primaryStage.setTitle("Step04");
		primaryStage.setResizable(false);
		primaryStage.setScene(scene);
		primaryStage.sizeToScene();
		primaryStage.centerOnScreen();
		
		primaryStage.show();
	}

}