public class Test {
	
	public static void main(String[] args) {
		Circle circle = new Circle(10);
		System.out.println("A circle");
		circle.printCircle();
		System.out.println("diameter = " + circle.getDiameter());
		System.out.println("area = " + circle.getArea());
		System.out.println("perimeter = " + circle.getPerimeter());

		System.out.println();
		
		Rectangle rectangle = new Rectangle(2, 4);
		System.out.println("A rectangle");
		rectangle.printRectangle();
		System.out.println("area = " + rectangle.getArea());
		System.out.println("perimeter = " + rectangle.getPerimeter());
	}
}