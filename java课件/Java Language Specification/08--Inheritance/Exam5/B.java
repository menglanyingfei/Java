public class B extends A {

	public B() {
		this(1);
		System.out.println("B()");
	}

	public B(int n) {
		super(n);
		System.out.println("B(int n)");
	}

}