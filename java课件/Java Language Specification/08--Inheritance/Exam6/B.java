public class B extends A {

	public B() {
		super(1);
		System.out.println("B()");
	}

	public B(int n) {
		this();
		System.out.println("B(int n)");
	}

}