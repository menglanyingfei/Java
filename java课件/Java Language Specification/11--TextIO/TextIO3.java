import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class TextIO3 {

	public static void main(String[] args) {
		String sno, name;
		int age;
		double s1, s2, s3;
		
		try (Scanner scanner = new Scanner(new File("student.txt"))) {
			for (int i = 0; i < 3; i++) {
				sno = scanner.next();
				name = scanner.next();
				age = scanner.nextInt();
				s1 = scanner.nextDouble();
				s2 = scanner.nextDouble();
				s3 = scanner.nextDouble();
				
				System.out.println(sno + " " + name + " " + age + " " + (s1 + s2 + s3) / 3);
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}