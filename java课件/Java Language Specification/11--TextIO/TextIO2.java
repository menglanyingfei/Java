import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class TextIO2 {

	public static void main(String[] args) {
		try (PrintWriter printWriter = new PrintWriter("student.txt")) {
			printWriter.print("2015001 ");
			printWriter.print("张三 ");
			printWriter.print(18 + " ");
			printWriter.println(85.0 + " " + 92.5 + " " + 95.5);
			
			printWriter.print("2015002 ");
			printWriter.print("李四 ");
			printWriter.print(19 + " ");
			printWriter.println(90.0 + " " + 85.5 + " " + 76.5);
			
			printWriter.print("2015003 ");
			printWriter.print("王五 ");
			printWriter.print(18 + " ");
			printWriter.println(83.5 + " " + 90.0 + " " + 87.5);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}