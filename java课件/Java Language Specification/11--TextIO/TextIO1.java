import java.util.Scanner;

public class TextIO1 {

	public static void main(String[] args) {
		try (Scanner scanner = new Scanner(System.in)) {
			String sno = scanner.next();
			String name = scanner.next();
			int age = scanner.nextInt();
			double s1 = scanner.nextDouble();
			double s2 = scanner.nextDouble();
			double s3 = scanner.nextDouble();

			System.out.println(sno + " " + name + " " + age + " " + (s1 + s2 + s3) / 3);
		}
	}

}