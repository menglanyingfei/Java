import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;


public class CopySource2 {

	public static void main(String[] args) {
		String sourceFileName = "radius.dat";
		
		System.out.println("准备复制文件");
		System.out.println();
		
		try (ServerSocket serverSocket = new ServerSocket(10000);
				Socket socket = serverSocket.accept();
				BufferedInputStream fileBIS = new BufferedInputStream(new FileInputStream(sourceFileName))) {
			BufferedOutputStream socketBOS = new BufferedOutputStream(socket.getOutputStream());
			byte[] b = new byte[1024];
			int len;
			while ((len = fileBIS.read(b)) != -1) {
				socketBOS.write(b, 0, len);
			}
			socketBOS.flush();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("复制文件完成");
	}

}