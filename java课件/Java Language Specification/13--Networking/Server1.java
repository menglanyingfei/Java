import java.io.DataInputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server1 {
	
	public static void main(String[] args) {
		System.out.println("服务器端开始");
		System.out.println();
		
		try (ServerSocket serverSocket = new ServerSocket(10000);
				Socket socket = serverSocket.accept()) {
			DataInputStream dis = new DataInputStream(socket.getInputStream());
			double radius = dis.readDouble();
			
			double area = Math.PI * radius * radius;

			System.out.println("radius = " + radius);
			System.out.println("area = " + area);
			System.out.println();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("服务器端结束");
	}

}