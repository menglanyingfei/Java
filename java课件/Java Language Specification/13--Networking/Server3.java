import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;


public class Server3 {

	public static void main(String[] args) {
		System.out.println("服务器端开始");
		System.out.println();
		
		try (ServerSocket serverSocket = new ServerSocket(10000);
				Socket socket = serverSocket.accept()) {
			DataInputStream dis = new DataInputStream(socket.getInputStream());
			DataOutputStream dos = new DataOutputStream(socket.getOutputStream());
			
			double radius = dis.readDouble(), area;
			while (radius > 0) {
				area = Math.PI * radius * radius;
				
				dos.writeDouble(area);
				
				radius = dis.readDouble();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("服务器端结束");
	}

}