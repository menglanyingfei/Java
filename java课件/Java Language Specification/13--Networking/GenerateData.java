import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class GenerateData {

	public static void main(String[] args) {
		try (DataOutputStream dos = new DataOutputStream(new BufferedOutputStream(new FileOutputStream("radius.dat")))) {
			for (int i = 0; i < 100; i++) {
				dos.writeDouble(Math.random() * 100);
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}