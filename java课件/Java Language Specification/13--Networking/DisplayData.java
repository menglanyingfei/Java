import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class DisplayData {

	public static void main(String[] args) {
		try (DataInputStream radiusDIS = new DataInputStream(new BufferedInputStream(new FileInputStream("radius.dat")));
				DataInputStream areaDIS = new DataInputStream(new BufferedInputStream(new FileInputStream("area.dat")))) {
			for (int i = 0; i < 100; i++) {
				System.out.println(i + 1);
				System.out.println("radius = " + radiusDIS.readDouble());
				System.out.println("area = " + areaDIS.readDouble());
				System.out.println();
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
