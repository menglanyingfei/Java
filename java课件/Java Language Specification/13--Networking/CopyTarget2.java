import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.Socket;


public class CopyTarget2 {

	public static void main(String[] args) {
		String targetFileName = "tmp.dat";
		
		System.out.println("开始复制文件...");
		long t = System.currentTimeMillis();
		
		try (Socket socket = new Socket("119.75.217.109", 10000);
				BufferedOutputStream fileBOS = new BufferedOutputStream(new FileOutputStream(targetFileName))) {
			BufferedInputStream socketBIS = new BufferedInputStream(socket.getInputStream());
			byte[] b = new byte[1024];
			int len;
			while ((len = socketBIS.read(b)) != -1) {
				fileBOS.write(b, 0, len);
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		t = System.currentTimeMillis() - t;
		System.out.println("耗时" + t / 1000 + "秒" + t % 1000 + "毫秒");
	}

}