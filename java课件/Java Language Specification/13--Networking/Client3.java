import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;


public class Client3 {

	public static void main(String[] args) {
		System.out.println("客户端开始");
		System.out.println();
		
		try (Scanner scanner = new Scanner(System.in);
				Socket socket = new Socket("127.0.0.1", 10000)) {
			DataOutputStream dos = new DataOutputStream(socket.getOutputStream());
			DataInputStream dis = new DataInputStream(socket.getInputStream());
			
			double radius = scanner.nextDouble(), area;
			while (radius > 0) {
				dos.writeDouble(radius);
				
				area = dis.readDouble();
				
				System.out.println("radius = " + radius);
				System.out.println("area = " + area);
				System.out.println();
				
				radius = scanner.nextDouble();
			}
			
			dos.writeDouble(radius);
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("客户端结束");
	}

}