import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;


public class Client2 {

	public static void main(String[] args) {
		System.out.println("客户端开始");
		System.out.println();
		
		try (Socket socket = new Socket("127.0.0.1", 10000)) {
			double radius = 10;
			
			DataOutputStream dos = new DataOutputStream(socket.getOutputStream());
			dos.writeDouble(radius);
			
			DataInputStream dis = new DataInputStream(socket.getInputStream());
			double area = dis.readDouble();
			
			System.out.println("radius = " + radius);
			System.out.println("area = " + area);
			System.out.println();
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("客户端结束");
	}

}