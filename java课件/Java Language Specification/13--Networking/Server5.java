import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;


public class Server5 {

	public static void main(String[] args) {
		System.out.println("服务器端开始");
		System.out.println();
		
		try (ServerSocket serverSocket = new ServerSocket(10000);
				Socket socket = serverSocket.accept()) {
			double[] radius = new double[100];
			DataInputStream dis = new DataInputStream(socket.getInputStream());
			for (int i = 0; i < radius.length; i++) {
				radius[i] = dis.readDouble();
			}

			DataOutputStream dos = new DataOutputStream(socket.getOutputStream());
			for (int i = 0; i < radius.length; i++) {
				dos.writeDouble(Math.PI * radius[i] * radius[i]);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("服务器端结束");
	}

}