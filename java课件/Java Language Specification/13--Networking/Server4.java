import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;


public class Server4 {

	public static void main(String[] args) {
		System.out.println("服务器端开始");
		System.out.println();
		
		try (ServerSocket serverSocket = new ServerSocket(10000);
				Socket socket = serverSocket.accept()) {
			DataInputStream dis = new DataInputStream(socket.getInputStream());
			DataOutputStream dos = new DataOutputStream(socket.getOutputStream());
			
			double radius;
			for (int i = 0; i < 100; i++) {
				radius = dis.readDouble();
				
				dos.writeDouble(Math.PI * radius * radius);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("服务器端结束");
	}

}