import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

public class Client4 {

	public static void main(String[] args) {
		long t = System.currentTimeMillis();
		
		System.out.println("客户端开始");
		System.out.println();
		
		try (DataInputStream fileDIS = new DataInputStream(new BufferedInputStream(new FileInputStream("radius.dat")));
				DataOutputStream fileDOS = new DataOutputStream(new BufferedOutputStream(new FileOutputStream("area.dat")));
				Socket socket = new Socket("119.75.217.109", 10000)) {
			DataOutputStream socketDOS = new DataOutputStream(socket.getOutputStream());
			DataInputStream socketDIS = new DataInputStream(socket.getInputStream());
			
			for (int i = 0; i < 100; i++) {
				socketDOS.writeDouble(fileDIS.readDouble());
				
				fileDOS.writeDouble(socketDIS.readDouble());
			}
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("客户端结束");
		System.out.println();
		
		t = System.currentTimeMillis() - t;
		System.out.println("耗时" + t / 1000 + "秒" + t % 1000 + "毫秒");
	}

}