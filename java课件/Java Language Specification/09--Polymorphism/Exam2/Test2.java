public class Test2 {

	public static void main(String[ ] args) {
		m(new Person());
		m(new Employee());
		m(new Faculty());
	}
	
	private static void m(Person person) {
		person.printInfo();
		System.out.println("info = " + person.info);
	}
	
}