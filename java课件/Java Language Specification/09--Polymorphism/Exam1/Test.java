public class Test {

	public static void main(String[] args) {
		Person person1 = new Person();
		Person person2 = new Employee();
		Person person3 = new Faculty();
		person1.printInfo();
		person2.printInfo();
		person3.printInfo();
	}

}