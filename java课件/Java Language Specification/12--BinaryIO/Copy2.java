import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class Copy2 {

	public static void main(String[] args) {
		System.out.println("开始复制文件...");
		long t = System.currentTimeMillis();
		
		try (FileInputStream fis = new FileInputStream("image//p01m.jpg");
				FileOutputStream fos = new FileOutputStream("tmp.jpg")) {
			byte[] b = new byte[1024];
			int len;
			while ((len = fis.read(b)) != -1) {
				fos.write(b, 0, len);
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		t = System.currentTimeMillis() - t;
		System.out.println("耗时" + t / 1000 + "秒" + t % 1000 + "毫秒");
	}

}