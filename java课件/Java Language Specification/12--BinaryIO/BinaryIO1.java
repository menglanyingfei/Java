import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class BinaryIO1 {

	public static void main(String[] args) {
		try (DataOutputStream dos = new DataOutputStream(new BufferedOutputStream(new FileOutputStream("student.dat")))) {
			dos.writeUTF("2015001");
			dos.writeUTF("张三");
			dos.writeInt(18);
			dos.writeDouble(85.0);
			dos.writeDouble(92.5);
			dos.writeDouble(95.5);
			
			dos.writeUTF("2015002");
			dos.writeUTF("李四");
			dos.writeInt(19);
			dos.writeDouble(90.0);
			dos.writeDouble(85.5);
			dos.writeDouble(76.5);
			
			dos.writeUTF("2015003");
			dos.writeUTF("王五");
			dos.writeInt(18);
			dos.writeDouble(83.5);
			dos.writeDouble(90.0);
			dos.writeDouble(87.5);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}