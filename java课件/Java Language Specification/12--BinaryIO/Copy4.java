import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class Copy4 {

	public static void main(String[] args) {
		System.out.println("开始复制文件...");
		long t = System.currentTimeMillis();
		
		try (BufferedInputStream bis = new BufferedInputStream(new FileInputStream("image//p01m.jpg"));
				BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream("tmp.jpg"))) {
			byte[] b = new byte[1024];
			int len;
			while ((len = bis.read(b)) != -1) {
				bos.write(b, 0, len);
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		t = System.currentTimeMillis() - t;
		System.out.println("耗时" + t / 1000 + "秒" + t % 1000 + "毫秒");
	}

}