import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class BinaryIO2 {

	public static void main(String[] args) {
		String sno, name;
		int age;
		double s1, s2, s3;
		
		try (DataInputStream dis = new DataInputStream(new BufferedInputStream(new FileInputStream("student.dat")))) {
			for (int i = 0; i < 3; i++) {
				sno = dis.readUTF();
				name = dis.readUTF();
				age = dis.readInt();
				s1 = dis.readDouble();
				s2 = dis.readDouble();
				s3 = dis.readDouble();
				
				System.out.println(sno + " " + name + " " + age + " " + (s1 + s2 + s3) / 3);
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}